#!/bin/bash

echo "Script: Installing ansible and python3"
apt update -y
apt install software-properties-common -y
add-apt-repository --yes --update ppa:ansible/ansible
apt install ansible python3 python3-pip -y

echo "Installing boto3 botocore"
pip install boto3 botocore

echo "checking if ~/.aws exist."
if [[ ! -d ~/.aws ]]; then
  echo "creating ~/.aws folder"
  mkdir ~/.aws
fi

if [[ ! -f ~/.aws/credentials ]]; then
  echo "creating ~/.aws/credentials"
  touch ~/.aws/credentials
  echo "[default]" > ~/.aws/credentials
fi

echo "populating the ~/.aws/credentials"
echo "aws_access_key_id = ${AWS_ACCESS_KEY_ID}" >> ~/.aws/credentials
echo "aws_secret_access_key = ${AWS_SECRET_ACCESS_KEY}" >> ~/.aws/credentials